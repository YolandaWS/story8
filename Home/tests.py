from django.test import TestCase,Client
from django.urls import resolve
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

class Story8UnitTest(TestCase):

	def test_story_8_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_story_6_using_story_6_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'Home/Home.html')

	def test_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_story_6_landing_page(self):
		response = Client().get('/')
		response_content = response.content.decode('utf8')
		self.assertIn("Profile", response_content)

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('chromedriver', chrome_options=chrome_options)
		super(Story8UnitTest, self).setUp()
    
	def test_input_message(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		
		selenium.implicitly_wait(5)

		up = selenium.find_element_by_class_name('button_up')
		down = selenium.find_element_by_class_name('button_down')

		up.click()
		down.click()

	def test_title_exist(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		self.assertEqual('Welcome-Story8', selenium.title)

	def tearDown(self):
		self.selenium.quit()
		super(Story8UnitTest, self).tearDown()

    


